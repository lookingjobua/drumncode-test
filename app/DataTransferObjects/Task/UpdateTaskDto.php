<?php

namespace App\DataTransferObjects\Task;

use App\Http\Requests\Task\UpdateTaskRequest;

/**
 * Update task DTO.
 */
class UpdateTaskDto extends TaskDto
{
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param integer $id
     * @return UpdateTaskDto
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return UpdateTaskDto
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return UpdateTaskDto
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     * @return UpdateTaskDto
     */
    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @param UpdateTaskRequest $request
     * @return self
     */
    public static function fromRequest(UpdateTaskRequest $request): self
    {
        $dto = new self();

        $dto->setId($request->id);
        $dto->setTitle($request->title);
        $dto->setDescription($request->description);
        $dto->setPriority($request->priority);

        return $dto;
    }
}
