<?php

namespace App\DataTransferObjects\Task;

use App\Http\Requests\Task\DeleteTaskRequest;

/**
 * Delete task DTO.
 */
class DeleteTaskDto extends TaskDto
{
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return DeleteTaskDto
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param DeleteTaskRequest $request
     * @return self
     */
    public static function fromRequest(DeleteTaskRequest $request): self
    {
        $dto = new self();

        $dto->setId($request->id);

        return $dto;
    }
}
