<?php

namespace App\DataTransferObjects\Task;

use App\Models\Task;
use Illuminate\Http\Request;

/**
 * Update task DTO.
 */
class CompleteTaskDto extends TaskDto
{
    /** @var int */
    private $id;

    /**
     * @param int $id
     * @return self
     */
    public static function make(int $id): self
    {
        $createProduct = new self();
        $createProduct->setId($id);

        return $createProduct;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param integer $id
     * @return UpdateTaskDto
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param Request $request
     * @return self
     */
    public static function fromRequest(Request $request): self
    {
        $dto = new self();
        $dto->setId($request->id);

        return $dto;
    }
}
