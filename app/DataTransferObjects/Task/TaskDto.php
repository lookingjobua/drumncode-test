<?php

namespace App\DataTransferObjects\Task;

/**
 * Task Dto class.
 */
abstract class TaskDto
{
    /** @var int */
    private $id;

    /** @var string */
    private $title;

    /** @var string */
    private $description;

    /** @var int */
    private $status;

    /** @var integer|null */
    private $parent_id;

    /** @var integer */
    private $priority;

    /** @var integer */
    private $user_id;

    /** @var mixed */
    private $subtasks;
}
