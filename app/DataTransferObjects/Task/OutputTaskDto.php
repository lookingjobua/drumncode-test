<?php

namespace App\DataTransferObjects\Task;

use App\Models\Task;

/**
 * Output task DTO.
 */
class OutputTaskDto extends CreateTaskDto
{
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param integer $id
     * @return UpdateTaskDto
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param Task $task
     * @return self
     */
    public static function fromModel(Task $task): self
    {
        $dto = new self();

        $dto->setId($task->id);
        $dto->setUserId($task->user_id);
        $dto->setPriority($task->priority);
        $dto->setTitle($task->title);
        $dto->setDescription($task->description);
        $dto->setParentId($task->parent_id);
        $dto->setStatus($task->status);
        $dto->setCreatedAt($task->created_at);
        $dto->setSubtasks($task->subtasks);

        return $dto;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'id' => $this->getId(),
            'user_id' => $this->getUserId(),
            'priority' => $this->getPriority(),
            'title' => $this->getTitle(),
            'description' => $this->getDescription(),
            'parent_id' => $this->getParentId(),
            'status' => $this->getStatus(),
            'created_at' => $this->getCreatedAt(),
            'subtasks' => $this->getSubtasks(),
        ];
    }
}
