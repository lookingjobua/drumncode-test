<?php

namespace App\DataTransferObjects\Task;

use App\Enums\TaskStatus;
use App\Http\Requests\Task\CreateTaskRequest;
use Carbon\Carbon;

/**
 * Create Task Dto class.
 */
class CreateTaskDto extends TaskDto
{
    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return CreateTaskDto
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return CreateTaskDto
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getParentId(): ?int
    {
        return $this->parent_id;
    }

    /**
     * @param int|null $parentId
     * @return CreateTaskDto
     */
    public function setParentId(?int $parentId): self
    {
        $this->parent_id = $parentId;

        return $this;
    }

    /**
     * @return int
     */
    public function getPriority(): ?int
    {
        return $this->priority;
    }

    /**
     * @param int $userId
     * @return CreateTaskDto
     */
    public function setUserId(int $userId): self
    {
        $this->user_id = $userId;

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $priority
     * @return CreateTaskDto
     */
    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return CreateTaskDto
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    /**
     * @param string $createdAt
     * @return CreateTaskDto
     */
    public function setCreatedAt(string $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSubtasks(): mixed
    {
        return $this->subtasks;
    }

    /**
     * @param mixed $subtasks
     * @return CreateTaskDto
     */
    public function setSubtasks(mixed $subtasks): self
    {
        $this->subtasks = $subtasks;

        return $this;
    }

    /**
     * @param CreateTaskRequest $request
     * @return self
     */
    public static function fromRequest(CreateTaskRequest $request): self
    {
        $dto = new self();

        $dto->setUserId(auth()->user()->id);
        $dto->setPriority($request->priority);
        $dto->setTitle($request->title);
        $dto->setDescription($request->description);
        $dto->setParentId($request->parentId);
        $dto->setStatus(TaskStatus::TODO->value);
        $dto->setCreatedAt(Carbon::now());

        return $dto;
    }

    /**
     * @param array $data
     * @return self
     */
    public static function fromArray(array $data): self
    {
        $dto = new self();

        $dto->setUserId($data['user_id']);
        $dto->setPriority($data['priority']);
        $dto->setTitle($data['title']);
        $dto->setDescription($data['description']);
        $dto->setParentId($data['parent_id'] ?? null);
        $dto->setStatus(TaskStatus::TODO->value);
        $dto->setCreatedAt(Carbon::now());
        $dto->setSubtasks($data['subtasks'] ?? null);

        return $dto;
    }
}
