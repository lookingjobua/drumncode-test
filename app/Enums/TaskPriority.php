<?php

namespace App\Enums;

use App\Traits\EnumTrait;

/**
 * Task priorities. Range: 1...5
 */
enum TaskPriority: int
{
    use EnumTrait;

    case UNKNOWN = 1;
    case LOW = 2;
    case NEUTRAL = 3;
    case HIGH = 4;
    case CRITICAL = 5;
}
