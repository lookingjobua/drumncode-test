<?php

namespace App\Enums;

use App\Traits\EnumTrait;

/**
 * TaskController statuses.
 */
enum TaskStatus: int
{
    use EnumTrait;

    /**
     * Incomplete task status.
     */
    case TODO = 0;

    /**
     * Completed task status.
     */
    case DONE = 1;
}
