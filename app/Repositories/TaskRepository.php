<?php

namespace App\Repositories;

use App\Models\Task;
use App\Enums\TaskStatus;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;

/**
 * Tasks repository class.
 */
class TaskRepository
{
    /**
     * @param Task $task
     */
    public function __construct(
        private readonly Task $task
    ) {
    }

    /**
     * @param int $id
     * @return ?Task
     * @throws ModelNotFoundException
     */
    public function findById(int $id): ?Task
    {
        return $this->task->newQuery()->where('id', $id)->get()->first();
    }

    /**
     * @param int $userId
     * @param int|null $status
     * @param int|null $priority
     * @param string|null $text
     * @param int|null $parentId
     * @param string|null $sortBy
     * @return Collection
     */
    public function findAll(
        int $userId,
        ?int $status,
        ?int $priority,
        ?string $text,
        ?int $parentId,
        ?string $sortBy
    ): Collection {
        $tasks = $this->task->newQuery()
            ->where('user_id', $userId);

        if (!empty($status)) {
            $tasks->where('status', $status);
        }
        if (!empty($priority)) {
            $tasks->where('priority', $priority);
        }
        if (!empty($parentId)) {
            $tasks->where('parent_id', $parentId);
        }
        if (!empty($text)) {
            $tasks->searchFullText($text);
        }
        if (is_null($sortBy)) {
            $sortBy = 'priority desc';
        }
        $tasks->orderByRaw($sortBy);

        return $tasks->get();
    }

    /**
     * @param int $id
     * @return Collection
     */
    public function findUncompletedSubtasks(int $id): Collection
    {
        return $this->task->newQuery()->find($id)->subtasks()->where('status', '=', TaskStatus::TODO->value)->get();
    }

    /**
     * @param Task $task
     * @return bool
     */
    public function save(Task $task): bool
    {
        return $task->save();
    }

    /**
     * @param Task $task
     * @return bool
     */
    public function delete(Task $task): bool
    {
        return $task->delete();
    }
}
