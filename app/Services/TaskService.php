<?php

namespace App\Services;

use App\DataTransferObjects\Task\CompleteTaskDto;
use App\DataTransferObjects\Task\CreateTaskDto;
use App\DataTransferObjects\Task\DeleteTaskDto;
use App\DataTransferObjects\Task\OutputTaskDto;
use App\DataTransferObjects\Task\UpdateTaskDto;
use App\Enums\TaskStatus;
use App\Models\Task;
use App\Repositories\TaskRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection as SupportCollection;
use Illuminate\Support\Str;

/**
 * Task service layer class.
 */
class TaskService
{
    /**
     * @param TaskRepository $repository
     */
    public function __construct(
        private readonly TaskRepository $repository
    ) {
    }

    /**
     * @param int $id
     * @return ?Task
     * @throws ModelNotFoundException
     */
    public function findById(int $id): ?Task
    {
        /** TODO: will we use cache? */
        return $this->repository->findById($id);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function findOrFail(int $id): JsonResponse
    {
        /** TODO: will we use cache? */
        /** @var Task $task */
        $task = $this->repository->findById($id);
        if (!$task) {
            return response()->json([
                'status' => false,
                'message' => __('Task not found.'),
            ], 404);
        }

        return response()->json([
            'status' => true,
            'data' => OutputTaskDto::fromModel($task)->toArray(),
        ]);
    }

    /**
     * @param Request $request
     * @param bool $asTree
     * @return JsonResponse
     */
    public function findAll(Request $request, bool $asTree = false): JsonResponse
    {
        try {
            /** @var Collection $tasks */
            $tasks = $this->repository->findAll(
                auth()->user()->id,
                $request->input('status'),
                $request->input('priority'),
                $request->input('text'),
                $request->input('parent_id'),
                $request->input('sortBy')
            );

            if ($asTree) {
                $this->getMissingParentTasks($tasks);
                $tasks = $this->buildTreeFromAll($tasks, $request->input('sortBy'))->values();
            }
        } catch (\Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => __('An error occurred while getting tasks.'),
                'error' => $exception->getMessage(),
            ], 500);
        }

        return response()->json([
            'status' => true,
            'data' => $tasks->map(function (Task $task) {
                return OutputTaskDto::fromModel($task);
            }),
        ]);
    }

    /**
     * Getting all missing parent tasks of each task.
     *
     * @param Collection $tasks
     * @return Collection
     */
    public function getMissingParentTasks(Collection &$tasks): Collection
    {
        /** @var Collection $tasks */
        $tasks = $tasks->keyBy('id');

        return $tasks->each(function (Task $task) use (&$tasks) {
            if (is_null($task->parent_id)) {
                return;
            }

            /** @var Task $parent */
            $parent = $tasks->get($task->parent_id);
            if (is_null($parent)) {
                /** TODO: will we use cache? */
                $parent = $this->repository->findById($task->parent_id);
                $tasks->push($parent);
                $this->getMissingParentTasks($tasks);
            }
        })->filter()->values();
    }

    /**
     * Build a tree from a flat list of tasks.
     *
     * @param Collection $tasks
     * @param string|null $sortBy
     * @return SupportCollection
     */
    protected function buildTreeFromAll(Collection $tasks, ?string $sortBy): SupportCollection
    {
        $sortedColumns = null;
        if (!is_null($sortBy)) {
            $sortedColumns = collect(explode(',', $sortBy))->map(function (string $sortByClause) {
                $sortByParts = collect(explode(' ', trim($sortByClause)));

                return collect()->push(
                    $sortByParts->get(0),
                    count($sortByParts) === 1 ? 'asc' : Str::lower($sortByParts->get(1))
                );
            });
        }

        $tasks->keyBy('id')->map(function (Task $task) use ($tasks, $sortBy, $sortedColumns) {
            if (is_null($task->parent_id)) {
                return $task;
            }

            $parent = $tasks->get($task->parent_id);
            if ($parent) {
                if (!$parent->relationLoaded('subtasks')) {
                    $parent->setRelation('subtasks', collect()->push($task));
                } else {
                    $parent->subtasks->push($task);
                    if (!is_null($sortedColumns)) {
                        $parent->subtasks->sortBy($sortedColumns);
                    }
                }
            }

            return null;
        })->filter()->values();

        return $tasks;
    }

    /**
     * @param int $id
     * @return bool
     */
    public function hasUncompletedSubtasks(int $id): bool
    {
        $subtasks = $this->repository->findUncompletedSubtasks($id);

        return $subtasks && $subtasks->count();
    }

    /**
     * @param CreateTaskDto $dto
     * @return JsonResponse
     */
    public function create(CreateTaskDto $dto): JsonResponse
    {
        try {
            $task = new Task();

            $task->user_id = $dto->getUserId();
            $task->parent_id = $dto->getParentId();
            $task->status = $dto->getStatus();
            $task->priority = $dto->getPriority();
            $task->title = $dto->getTitle();
            $task->description = $dto->getDescription();
            $task->created_at = $dto->getCreatedAt();

            $this->repository->save($task);
            /** TODO: will we use cache? */
        } catch (\Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => __('An error occurred during creating new task.'),
                'error' => $exception->getMessage(),
            ], 500);
        }

        return response()->json([
            'status' => true,
            'message' => __('Task created successfully.'),
            'data' => OutputTaskDto::fromModel($task),
        ], 201);
    }

    /**
     * @param SupportCollection $collection
     * @param int $userId
     * @param null $parent
     * @return JsonResponse
     */
    public function createFromCollection(\Illuminate\Support\Collection $collection, int $userId, $parent = null): JsonResponse
    {
        try {
            $collection->map(function ($item) use ($userId, $parent) {
                $dto = CreateTaskDto::fromArray(array_merge($item, [
                    'user_id' => $userId,
                    'parent_id' => !is_null($parent) ? $parent->id : null,
                ]));

                $task = new Task();

                $task->user_id = $dto->getUserId();
                $task->parent_id = $dto->getParentId();
                $task->status = $dto->getStatus();
                $task->priority = $dto->getPriority();
                $task->title = $dto->getTitle();
                $task->description = $dto->getDescription();
                $task->created_at = $dto->getCreatedAt();

                $this->repository->save($task);

                if (isset($item['subtasks'])) {
                    $this->createFromCollection(collect($item['subtasks']), $userId, $task);
                }
            });

        } catch (\Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => __('An error occurred during creating new tasks from collection.'),
                'error' => $exception->getMessage(),
            ], 500);
        }

        return response()->json([
            'status' => true,
            'message' => __('Tasks created successfully.'),
        ], 201);
    }

    /**
     * @param UpdateTaskDto $dto
     * @return JsonResponse
     */
    public function update(UpdateTaskDto $dto): JsonResponse
    {
        /** TODO: will we use cache? */
        /** @var Task $task */
        $task = $this->repository->findById($dto->id);

        if (!$task) {
            return response()->json([
                'status' => false,
                'message' => __('Task not found.'),
            ], 404);
        }
        if ($task->user_id !== auth()->user()->id) {
            return response()->json([
                'status' => false,
                'message' => __('Access denied.'),
            ], 403);
        }

        $task->priority = $dto->getPriority();
        $task->title = $dto->getTitle();
        $task->description = $dto->getDescription();

        try {
            $this->repository->save($task);
            $task->fresh();
        } catch (\Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => __('An error occurred during update the task.'),
                'error' => $exception->getMessage(),
            ], 500);
        }

        return response()->json([
            'status' => true,
            'message' => __('Task updated successfully.'),
            'data' => OutputTaskDto::fromModel($task),
        ], 202);
    }

    /**
     * @param CompleteTaskDto $dto
     * @return JsonResponse
     */
    public function complete(CompleteTaskDto $dto): JsonResponse
    {
        /** TODO: will we use cache? */
        /** @var Task $task */
        $task = $this->repository->findById($dto->getId());
        if (!$task) {
            return response()->json([
                'status' => false,
                'message' => __('Task not found.'),
            ], 404);
        }
        if ($task->user_id !== auth()->user()->id) {
            return response()->json([
                'status' => false,
                'message' => __('Access denied.'),
            ], 403);
        }

        /** @var Collection $subtasks */
        $allSubtasksCompleted = true;
        $subtasks = $task->subtasks();
        $subtasks->each(function (Task $task) use (&$allSubtasksCompleted) {
            if ($task->status !== TaskStatus::DONE->value) {
                $allSubtasksCompleted = false;
            }
        });
        if (!$allSubtasksCompleted) {
            return response()->json([
                'status' => false,
                'message' => __('Unable to complete the task with uncompleted subtasks.'),
            ], 409);
        }

        try {
            $task->status = TaskStatus::DONE->value;
            $this->repository->save($task);
            $task->fresh();

            // Update parent task if all its subtasks are completed.
            if (!is_null($task->parent_id)) {
                /** @var Task $parent */
                $parent = $this->repository->findById($task->parent_id);
                /** @var Collection $subtasksOfParentTodo */
                $subtasksOfParentTodo = $parent->subtasks()->where('status', TaskStatus::TODO)->get();
                if ($parent->subtasks()->count() && !$subtasksOfParentTodo->count()) {
                    $parent->status = TaskStatus::DONE->value;
                    $this->repository->save($parent);
                }
            }
        } catch (\Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => __('An error occurred during update the task status.'),
                'error' => $exception->getMessage(),
            ], 500);
        }

        return response()->json([
            'status' => true,
            'message' => __('Task completed successfully.'),
            'data' => OutputTaskDto::fromModel($task),
        ], 202);
    }

    /**
     * @param DeleteTaskDto $dto
     * @return JsonResponse
     * @throws ModelNotFoundException
     */
    public function delete(DeleteTaskDto $dto): JsonResponse
    {
        /** TODO: will we use cache? */
        /** @var Task $task */
        $task = $this->repository->findById($dto->getId());
        if (!$task) {
            return response()->json([
                'status' => false,
                'message' => __('Task not found.'),
            ], 404);
        }
        if ($task->user_id !== auth()->user()->id) {
            return response()->json([
                'status' => false,
                'message' => __('Access denied.'),
            ], 403);
        }
        if ($task->status === TaskStatus::DONE->value) {
            return response()->json([
                'status' => false,
                'message' => __('Unable to delete completed task.'),
            ], 409);
        }
        if ($this->hasUncompletedSubtasks($dto->getId())) {
            return response()->json([
                'status' => false,
                'message' => __('Unable to delete task that has uncompleted subtasks.'),
            ], 409);
        }

        try {
            $this->repository->delete($task);
        } catch (\Exception $exception) {
            return response()->json([
                'status' => false,
                'message' => __('An error occurred during deleting the task.'),
                'error' => $exception->getMessage(),
            ], 500);
        }

        return response()->json([
            'status' => true,
            'message' => __('Task deleted successfully.'),
        ]);
    }
}
