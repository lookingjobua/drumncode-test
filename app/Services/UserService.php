<?php

namespace App\Services;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use LogicException;

/**
 * User service layer class.
 */
class UserService
{
    /**
     * @param User $user
     */
    public function __construct(
        private readonly User $user
    ) {
    }

    /**
     * @param int $id
     * @return ?User
     * @throws ModelNotFoundException
     */
    public function findById(int $id): ?User
    {
        $tasks = $this->user->newQuery()->where('id', $id)->get();

        return $tasks?->first();
    }

    /**
     * @param string $email
     * @return ?User
     */
    public function findByEmail(string $email): ?User
    {
        $task = $this->user->newQuery()->where('email', $email)->get();

        return $task?->first();
    }

    /**
     * @param string $name
     * @param string $email
     * @param string $password
     * @return User
     */
    public function create(string $name, string $email, string $password): User
    {
        $user = new User();

        $user->name = $name;
        $user->email = $email;
        $user->password = $password;
        $user->save();

        return $user;
    }
}
