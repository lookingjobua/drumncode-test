<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Collection;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        // Register a collection recursive macro.
        Collection::macro('recursive', function (array|object|null $parent) {
            return $this->map(function ($item) {
                if (is_array($item) || is_object($item)) {
                    return collect($item)->recursive($item);
                }

                return $item;
            });
        });
    }
}
