<?php

namespace App\Helpers;

use Illuminate\Support\Arr;
use Illuminate\Support\Collection;

/**
 * Fixture Helper class.
 */
class FixtureHelper
{
    /**
     * Load fixture from JSON file.
     *
     * @param string $fixtureName
     * @param string|null $key
     * @return Collection
     */
    public static function loadCollectionFromJson(string $fixtureName, ?string $key): Collection
    {
        $result = collect();
        $filePath = sprintf('%s/tests/fixtures/%s.json', base_path(), $fixtureName);
        if (!file_exists($filePath)) {
            return $result;
        }

        try {
            $content = file_get_contents($filePath);
            $data = json_decode($content, true);

            if (!is_null($data) && !is_null($key) && array_key_exists($key, $data)) {
                $result = collect($data[$key]);
            }
        } catch (\Exception $e) {
            //
        }

        return $result;
    }

    public function map($array)
    {
        $result = [];
        foreach ($array as $item) {
            $result[] = $item['accessables'];
            $result = array_merge($result, $this->mapRecursive($item['allChildren']));
        }
        return array_filter($result);
    }
}
