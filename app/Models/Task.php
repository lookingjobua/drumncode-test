<?php

namespace App\Models;

use App\Traits\FullTextSearchTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Task extends Model
{
    use HasFactory;
    use FullTextSearchTrait;

    /**
     * @inheritdoc
     */
    protected $table = 'tasks';

    /**
     * @inheritdoc
     */
    protected $fillable = [
        'user_id',
        'parent_id',
        'status',
        'priority',
        'title',
        'description',
        'created_at',
        'completed_at',
    ];

    /**
     * @inheritdoc
     */
    protected $visible = [
        'id',
        'parent_id',
        'status',
        'priority',
        'title',
        'description',
        'created_at',
        'completed_at',
        'subtasks',
    ];

    /**
     * The columns of the full text index.
     */
    protected $fullTextSearchable = [
        'title',
        'description',
    ];

    /**
     * Get the user of the task.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Parent task of the task.
     *
     * @return BelongsTo
     */
    public function parent(): BelongsTo
    {
        return $this->belongsTo(self::class, 'parent_id', 'id');
    }

    /**
     * Subtasks of the task.
     *
     * @return HasMany
     */
    public function subtasks(): HasMany
    {
        return $this->hasMany(self::class, 'parent_id', 'id');
    }

    public function scopeStatus($query, int $status)
    {
        return $query->where('status', 1);
    }

    public function scopeTitle($query, $name)
    {
        return $query->where('LIKE', '%' . $name . '%');
    }
}
