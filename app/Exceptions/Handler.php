<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Auth\AuthenticationException;
use Throwable;
use Illuminate\Foundation\Application;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Http\RedirectResponse;

/**
 *
 */
class Handler extends ExceptionHandler
{
    /**
     * The list of the inputs that are never flashed to the session on validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     */
    public function register(): void
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /**
     * @param $request
     * @param AuthenticationException $exception
     * @return Application|Response|JsonResponse|Redirector|RedirectResponse
     */
    protected function unauthenticated(
        $request,
        AuthenticationException $exception
    ): Application|Response|JsonResponse|Redirector|RedirectResponse {
        if ($request->is('api/*')) {
            return response()->json([
                'success' => false,
                'message' => $exception->getMessage()
            ], 401);
        }

        return redirect('/login');
    }
}
