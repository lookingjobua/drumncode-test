<?php

namespace App\Traits;

trait EnumTrait
{
    /**
     * @return string
     */
    public static function values(): array
    {
        return array_map(function ($item) {
            return $item->value;
        }, self::cases());
    }

    /**
     * @return string
     */
    public static function toString(): string
    {
        return implode(',', self::values());
    }
}
