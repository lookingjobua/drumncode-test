<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

trait FullTextSearchTrait
{
    /**
     * Replaces spaces with full text search wildcards.
     *
     * @param string $clause
     * @return string
     */
    protected function fullTextWildcards(string $clause): string
    {
        $reservedSymbols = ['-', '+', '<', '>', '@', '(', ')', '~'];
        $clause = str_replace($reservedSymbols, '', $clause);
        $words = explode(' ', $clause);

        foreach ($words as $key => $word) {
            if (strlen($word) >= 3) {
                $words[$key] = "{$word}*";
                //$words[$key] = '+' . $word . '*';
            }
        }

        return implode(' ', $words);
    }

    /**
     * Scope a query that matches a full text search of clause.
     *
     * @param Builder $query
     * @param string $clause
     * @return Builder
     */
    public function scopeSearchFullText(Builder $query, string $clause): Builder
    {
        $columns = collect($this->fullTextSearchable)->map(function ($column) {
            return $this->qualifyColumn($column);
        })->implode(',');
        $columns = implode(',', $this->fullTextSearchable);

        $query->whereRaw("MATCH ({$columns}) AGAINST (? IN BOOLEAN MODE)", $this->fullTextWildcards($clause));

        return $query;
    }
}
