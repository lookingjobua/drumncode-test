<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\User\LoginUserRequest;
use App\Http\Requests\User\RegisterUserRequest;
use Illuminate\Support\Facades\Auth;

/**
 * User controller.
 */
class AuthController extends Controller
{
    /**
     * @param UserService $userService
     */
    public function __construct(
        private readonly UserService $userService
    ) {
    }

    /**
     * @param RegisterUserRequest $request
     * @return JsonResponse
     */
    public function register(RegisterUserRequest $request): JsonResponse
    {
        $user = $this->userService->create($request->name, $request->email, $request->password);

        return response()->json([
            'status' => true,
            'message' => __('User created successfully.'),
        ]);
    }

    /**
     * @param LoginUserRequest $request
     * @return JsonResponse
     */
    public function login(LoginUserRequest $request): JsonResponse
    {
        if (!Auth::attempt($request->only(['email', 'password']))) {
            return response()->json([
                'status' => false,
                'message' => __('Invalid credentials.'),
            ], 401);
        }

        $user = $this->userService->findByEmail($request->email);

        return response()->json([
            'status' => true,
            'message' => __('You logged in successfully.'),
            'token' => $user->createToken('AuthToken')->plainTextToken
        ]);
    }
}
