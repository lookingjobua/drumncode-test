<?php

namespace App\Http\Controllers;

use App\DataTransferObjects\Task\CompleteTaskDto;
use App\DataTransferObjects\Task\CreateTaskDto;
use App\DataTransferObjects\Task\DeleteTaskDto;
use App\DataTransferObjects\Task\UpdateTaskDto;
use App\Http\Requests\Task\CompleteTaskRequest;
use App\Http\Requests\Task\CreateTaskRequest;
use App\Http\Requests\Task\DeleteTaskRequest;
use App\Http\Requests\Task\UpdateTaskRequest;
use App\Services\Taskservice;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

/**
 * TaskController class.
 */
class TaskController extends Controller
{
    /**
     * @param Taskservice $taskService
     */
    public function __construct(
        private readonly TaskService $taskService
    ) {
    }

    /**
     * Lists the tasks along with subtasks.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return $this->taskService->findAll($request, true);
    }

    /**
     * Creating new task.
     *
     * @param CreateTaskRequest $request
     * @return JsonResponse
     */
    public function store(CreateTaskRequest $request): JsonResponse
    {
        $dto = CreateTaskDto::fromRequest($request);

        return $this->taskService->create($dto);
    }

    /**
     * Update existing task by id.
     *
     * @param UpdateTaskRequest $request
     * @return JsonResponse
     */
    public function update(UpdateTaskRequest $request): JsonResponse
    {
        $dto = UpdateTaskDto::fromRequest($request);

        return $this->taskService->update($dto);
    }

    /**
     * Marks a task as complete.
     *
     * @param CompleteTaskRequest $request
     * @return JsonResponse
     */
    public function complete(CompleteTaskRequest $request): JsonResponse
    {
        $dto = CompleteTaskDto::fromRequest($request);

        return $this->taskService->complete($dto);
    }

    /**
     * Delete the task by id.
     *
     * @param DeleteTaskRequest $request
     * @return JsonResponse
     */
    public function delete(DeleteTaskRequest $request): JsonResponse
    {
        $dto = DeleteTaskDto::fromRequest($request);

        return $this->taskService->delete($dto);
    }
}
