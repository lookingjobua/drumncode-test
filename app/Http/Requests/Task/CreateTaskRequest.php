<?php

namespace App\Http\Requests\Task;

use App\Enums\TaskPriority;
use App\Http\Requests\BaseRequest;
use Illuminate\Validation\Rule;

class CreateTaskRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => ['required', 'string', 'max:255'],
            'description' => ['required', 'string'],
            'priority' => ['required', 'integer', Rule::in(TaskPriority::values())],
            'parentId' => 'sometimes|integer|exists:tasks,id',
        ];
    }
}
