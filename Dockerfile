FROM php:8.1-fpm

ARG user
ARG uid

RUN apt-get update && apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip

RUN apt-get clean && rm -rf /var/lib/apt/lists/*

RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

RUN useradd -G www-data,root -u $uid -d /home/$user $user
RUN mkdir -p /home/$user/.composer && \
    chown -R $user:$user /home/$user

COPY . /var/www
COPY --chown=$user:$user . /var/www

WORKDIR /var/www

USER $user

#RUN chown -R $user:$user /var/www && \
RUN chmod 777 -R /var/www/storage/


RUN php artisan config:cache && \
    php artisan route:cache && \
    composer install --no-dev --optimize-autoloader && \
    php artisan key:generate
