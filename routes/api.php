<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TaskController;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/open-api.json', function () {
    return response()->download('../open-api.json', null, [
        'Content-Type: application/json'
    ]);
});

Route::post('/auth/register', [AuthController::class, 'register']);
Route::post('/auth/login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    Route::get('/tasks', [TaskController::class, 'index']);

    Route::post('/tasks/create', [TaskController::class, 'store']);

    Route::put('/tasks/{id}/update', [TaskController::class, 'update'])
        ->whereNumber('id');
    Route::put('/tasks/{id}/complete', [TaskController::class, 'complete'])
        ->whereNumber('id');

    Route::delete('/tasks/{id}/delete', [TaskController::class, 'delete'])
        ->whereNumber('id');
});


