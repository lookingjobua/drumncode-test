<?php

namespace Database\Factories;

use App\Enums\TaskPriority;
use App\Enums\TaskStatus;
use App\Models\Task;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Task>
 */
class TaskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $users = User::all();
        if (!$users->count()) {
            throw new \Exception('No users found');
        }
        $user = $users->random();

        $userTasks = Task::where('user_id', $user->id)->get();

        return [
            'user_id' => $user->id,
            'title' => fake()->paragraph(1),
            'description' => fake()->paragraph(),
            'priority' => fake()->randomElement(TaskPriority::values()),
            'status' => fake()->randomElement(TaskStatus::values()),
            'parent_id' => fake()->randomElement([null, $userTasks->count() ? $userTasks->random()->id : null]),
        ];
    }
}
