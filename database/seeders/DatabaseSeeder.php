<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Helpers\FixtureHelper;
use App\Models\Task;
use App\Models\User;
use App\Services\TaskService;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * @param Taskservice $taskService
     */
    public function __construct(
        private readonly TaskService $taskService
    ) {
    }

    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        User::factory(20)->create([
            'password' => '12345678',
        ])->each(function (User $user) {
            $tasksCollection = FixtureHelper::loadCollectionFromJson('tasks', 'tree');
            $this->taskService->createFromCollection($tasksCollection, $user->id);

            Task::factory(10)->create([
                'user_id' => $user->id,
            ]);
        });

        /*User::factory()->create([
             'name' => 'Test User',
             'email' => 'test@example.com',
             'password' => '12345678',
        ]);*/

        Task::factory(200)->create();

        /*Task::factory()->create([
             'title' => 'Test Task',
             'description' => 'Test Task Description',
        ]);*/
    }
}
