<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreignId('parent_id')->nullable()->references('id')->on('tasks')->onDelete('cascade');
            $table->smallInteger('status');
            $table->index('status');
            $table->smallInteger('priority');
            $table->index('priority');
            $table->string('title');
            $table->text('description');
            $table->fullText(['title', 'description']);
            $table->timestamps();
            $table->index('created_at');
            $table->timestamp('completed_at')->nullable();
            $table->index('completed_at');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tasks');
    }
};
